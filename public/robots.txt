User-agent: *
Disallow:

Allow: /

User-agent: *
Disallow: /admin/
Disallow: /ajax/
Disallow: /assets/
Disallow: /css/
Disallow: /js/
Disallow: /vendor/

Sitemap: http://localhost:8000/en/br/sitemaps.xml
